import json

from django.core.mail import send_mail
from django.template import Template, Context


from dcim.models import Location, Device, Rack
from extras.reports import Report

# Need to check locations without devices
CHECK_DEVICES = True

# Need to check locations without racks
CHECK_RACKS = True

# Parameteres for filtering devices.
# In the case of an empty dictionary, all devices will be returned.
# -> (equal to .all() method)
FILTER = {
    #'device_role__slug': 'l3-switch',
    #'name__startswith': 'sw',
    #'serial': '123456789'
}

#Timeout for the report run
JOB_TIMEOUT = 300

# Need for sending the Email about the generated report
SEND_EMAIL = False

# Parametres for Email, if the variable SEND_EMAIL is True

EMAIL = {
    'subject': 'NetBox: Check empty Locations Report',
    'from_email': 'netbox@example.com',
    'recipient_list': [
       "user@domain.com"
    ]
}


# Template for Email written in Jinja2
EMAIL_FORMAT = """
{% for keys, values in data.items %}
{{ keys|title }}:
            INFO({{ values.info|safe }})
                    {% spaceless %}
                {% for name in values.log %}
                    {% spaceless %}
                        {% if name.1 == 'info' %}
                            {{ name.2|safe }}  -  {{ name.3|safe }}
                        {% endif %}
                    {% endspaceless %}
                {% endfor %}
                    {% endspaceless %}
            SUCCESS({{ values.success|safe }})
                    {% spaceless %}
                {% for name in values.log %}
                    {% spaceless %}
                        {% if name.1 == 'success' %}
                            {{ name.2|safe }}  -  {{ name.3|safe }}
                        {% endif %}
                    {% endspaceless %}
                {% endfor %}
                    {% endspaceless %}
            FAILURE({{ values.failure|safe }})
                    {% spaceless %}
                {% for name in values.log %}
                    {% spaceless %}
                        {% if name.1 == 'failure' %}
                            {{ name.2|safe }}  -  {{ name.3|safe }}
                        {% endif %}
                    {% endspaceless %}
                {% endfor %}
                    {% endspaceless %}
            WARNING({{ values.warning|safe }})
                {% spaceless %}
            {% for name in values.log %}
                {% spaceless %}
                    {% if name.1 == 'warning' %}
                        {{ name.2|safe }}  -  {{ name.3|safe }}
                    {% endif %}
                {% endspaceless %}
            {% endfor %}
                {% endspaceless %}
{% endfor %}
"""

def order_log(func):
    def wrapper(self, *args, **kwargs):
        result = {}
        func(self, result)
        for log_type in ['log', 'log_failure', 'log_warning', 'log_info', 'log_success']:
            attr = result.get(log_type)
            if attr is not None:
                for record in attr:
                    getattr(self, log_type)(*record)

    return wrapper

class DeviceReport(Report):
    description = "Check empty Locations."
    job_timeout = JOB_TIMEOUT

    @property
    def name(self):
        return "Empty locations report."
    
    @order_log
    def test_devices(self, result):
        if not CHECK_DEVICES:
            result['log'] = result.get('log', []) + [('This test is not performed. (CHECK_DEVICES = False)', )]
            return
        
        for location in Location.objects.filter(**FILTER):
            device_count = Device.objects.filter(location_id = location.id).count()
            if device_count == 0:
                result['log_failure'] = result.get('log_failure', []) + [(location,f"Location doesn't have any devices.", )]

    @order_log
    def test_racks(self, result):
        if not CHECK_RACKS:
            result['log'] = result.get('log', []) + [('This test is not performed. (CHECK_RACKS = False)', )]
            return
        
        for location in Location.objects.filter(**FILTER):
            device_count = Rack.objects.filter(location_id = location.id).count()
            if device_count == 0:
                result['log_failure'] = result.get('log_failure', []) + [(location,f"Location doesn't have any racks.", )]

    def post_run(self):
        data = json.dumps(self._results)
        if SEND_EMAIL:
            email_template = Template(EMAIL_FORMAT)
            context = Context(
                {
                    'data': json.loads(data)
                }
            )
            message = email_template.render(context)
            send_mail(
                message=message,
                **EMAIL
            )

