# Check empty Locations

## Popis
Tento report je určen pro kontrolu absence objektů Device nebo objektů Rack v objektu Location.

## Instalace
- **[Verze 3.4.X a níže]** Report je nutno vložit do složky reports, která má nejčastěji tuto cestu netbox/reports/.
- **[Verze 3.5.X a výše]** Report je nutné naimportovat z lokální složky. Cesta v Netboxu je Customization -> Reports -> Add
- Viz https://docs.netbox.dev/en/stable/customization/reports/

## Výstup
![alt text](/images/check-empty-locations.png)

## Prerekvizity
1. Vytvořen objekt Site
2. Vytvořen objekt Location
3. Vyplněné globální proměnné v reportu

## Postup fungování Reportu
1. Report nejprve projde všechny objekty Location po aplikování filtru.
2. Podle uživatelských nastavení proběhnou kontroly absence objektů Device nebo objektů Rack v objektu Location. V případě, že uživatel nenastavil určitou kontrolu, je o této skutečnosti informován.
3. Kontrola objektů `Rack` ověřuje, zda existují objekty `Location`, které nemají objekt `Rack`.
4. Kontrola objektů `Device` ověřuje, zda existují objekty `Location`, které nemají objekt `Device`.
6. Report následně vypíše seskupené Failure logy podle kontrol.
7. Na závěr v případě nastavení, se na konci odešle email adresátům o výsledku Reportu.

## Proměnné
**Pro správné fungování reportu je nutné upravit globální proměnné.**

#### CHECK_DEVICES
 - Tato proměnná signalizuje, zdali se mají kontrolovat objekty `Location`, které nemají objekt `Device`.
 - Report zahlásí chybu, pokud daný Location nemá objekt `Device`.
 - Výchozí hodnota je nastavena na True.
```python
CHECK_DEVICES = True
```

#### CHECK_RACKS
 - Tato proměnná signalizuje, zdali se mají kontrolovat objekty `Location`, které nemají objekt `Rack`.
 - Report zahlásí chybu, pokud daný Location nemá objekt `Rack`.
 - Výchozí hodnota je nastavena na True.
```python
CHECK_RACKS = True
```

#### FILTER
 - Táto proměnná představuje atributy, podle kterých se má výběr locations filtrovat. 
 - V případě ponechání prázdných složených závorek nebude použit žádný filtr.
 - Výchozí hodnota je nastavena na Null.
```python
FILTER = {
    "tenant__name" : "Prague",
}
```
#### JOB_TIMEOUT
- Táto proměnna představuje timeout pro běh reportu.
- Výchozí hodnota je nastavena na 300 vteřin.
```python
JOB_TIMEOUT = 300
```
#### SEND_EMAIL
 - Tato proměnná představuje možnost posílání emailu o stavu reportu po jeho dokončení. 
 - Výchozí hodnota je nastavena na False.
 ```python
SEND_EMAIL = False
 ```

#### EMAIL
 - Táto proměnná představuje dictionary s parametry pro správné odesílání emailů. 
 - V kódu reportu se skládá z následujících parametrů:
    - **subject** - Předmět emailu
    - **from_email** - Odesílatel emailu
    - **recipient_list** - list příjemců mailu
    - Další parametry se nacházejí v konfiguraci NetBoxu v souboru configuration.py 
        - Dokumentace: **https://docs.djangoproject.com/en/4.2/topics/email/**
```python
EMAIL = {
    'subject': 'NetBox: Check empty Locations Report',
    'from_email': 'netbox@example.com',
    'recipient_list': [
       "user@domain.com"
    ],
}
```

#### EMAIL_FORMAT
 - Tato proměnná představuje formátování pro zaslaný email.
 - Pro zpracování je použita šablona se syntaxi Jinja v Django provedení.
 - Výsledek reportů je do šablony vložen přes proměnnou s názvem `data`.
```python
EMAIL_FORMAT = """

"""
```
_**Vzorový email**_
```python
Test_Devices:
            INFO(0)
                   
            SUCCESS(0)
                   
            FAILURE(2)
                    Room X  -  /dcim/locations/4/
               
                    Room Z  -  /dcim/locations/3/
            WARNING(0)
               

Test_Racks:
            INFO(0)
                   
            SUCCESS(0)
                   
            FAILURE(4)
                    Room ABC  -  /dcim/locations/2/
               
                    Room X  -  /dcim/locations/4/
               
                    Room XY  -  /dcim/locations/1/
               
                    Room Z  -  /dcim/locations/3/
            WARNING(0)

```

**Pro správné fungování je nutně vyplnění atributů v .env souboru "netbox.env"**
```python
EMAIL_FROM=netbox@domain.com
EMAIL_PASSWORD=
EMAIL_PORT=25
EMAIL_SERVER=relay.domain.com
EMAIL_SSL_CERTFILE=
EMAIL_SSL_KEYFILE=
EMAIL_TIMEOUT=5
EMAIL_USERNAME=
```
